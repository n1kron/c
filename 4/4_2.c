#include <stdio.h>
#include <stdlib.h>

int main()
{

	char str[256];
	int InWord = 0;
	int i = 0;
	int j = 0;
	int Words = 0;
	char *p[256] = { 0 };

	puts("Please enter a string:");
	fgets(str, 256, stdin);

	str[strlen(str) - 1] = 0;

	while (str[i])
	{
		if (str[i] != ' ' && InWord == 0)
		{
			p[j++] = &str[i];
			Words++;
			InWord = 1;
		}
		else if (str[i] == ' ' && InWord == 1)
		{
			InWord = 0;
		}

		i++;
	}
	
	for (i = 0; i < Words; i++)
	{
		InWord = 1;
		for (j = 0; j < strlen(p[i]); j++)
		{
			if (p[i][j] == '\n')
				p[i][j] = ' ';
			if (p[i][j] == ' ')
				InWord = 0;
			if (InWord == 0 && p[i][j+1] != ' ')
			{
				p[i][j] = '\0';
				break;
			}
		}
	}
	
	for (i = Words-1; i >= 0; i--)
		printf("%s ", p[i]);

	printf("\nWords count: %d\n",Words);
	
	return 0;
} 