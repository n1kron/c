#include <stdio.h>
#include <stdlib.h>

int main()
{

	char str[256];
	char *begin = { 0 };
	char *end = { 0 };
	puts("Please enter a string:");
	fgets(str, 256, stdin);

	str[strlen(str) - 1] = 0;
	end = &str[strlen(str) - 1];
	begin = &str;
	while (begin!=end)
	{
		
		if (*begin != *end)
		{
			printf("Not a palindrome\n");
			return 0;
		}
		begin++;
		end--;
	}
	printf("Palindrome\n");
	
	return 0;
} 