#include <stdio.h>

void swap(char *begin, char *end)
{
	while (*end)
	{
		*begin++ = *end++;
	}
	*begin = 0;

};

int main()
{
	char tempvar;
	char str[256];
	int i = 0;
	int j = 0;
	int k = 0;
	int multiplier = 1;
	int count = 0;
	int sum = 0;
	char maxw[256] = { 0 };
	puts("Enter a string, please:");
	fgets(str, 256, stdin);
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;

	while (str[i])
	{
		if (str[i]<48 || str[i]>57)
			str[i] = ' ';
		i++;
	}


	while (str[0] == ' ')
	{
		if (str[0] == ' ')
			swap(&str[0], &str[1]);
	}

	for (j = 0; j < strlen(str); j++)
	{
		for (i = 0; i < strlen(str); i++)
		{
			if (str[i] == ' ' && str[i + 1] == ' ')
				swap(&str[i], &str[i + 1]);
		}
	}

	printf("String without extra spaces and symbols:\n%s\n\n", str);

	i = 0;
	count = 0;
	for (i = 0; i < strlen(str); i++)
	{
		if (str[i] != ' ')
			count++;
		else
			count = 0;

		if (count>5)
		{
			for (k = strlen(str) + 1; k > i; k--)
			{
				tempvar = str[k - 1];
				str[k] = tempvar;
			}
			str[i] = ' ';
			count = 0;
		}
	}

	printf("String without long numbers:\n%s\n\n", str);

	str[strlen(str)] = ' ';

	i = 0;
	j = 0;
	count = 0;
	while (str[i])
	{
		if (str[i] != ' ')
		{
			count++;
			maxw[j] = str[i];
		}
		else
		{
			while (count)
			{
				sum = sum + (maxw[count - 1] - '0')* multiplier;
				multiplier *= 10;
				count--;
			}
			multiplier = 1;
			j = -1;
		}
		i++;
		j++;
	}

	printf("Sum: %d\n", sum);


	return 0;
}