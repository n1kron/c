#include <stdio.h>
#include <time.h>


int main()
{
	int size = 10;
	int min = 0;
	int minpos = 0;
	int max = 0;
	int maxpos = 0;
	int arr[10] = {0};
	int i = 0;
	int sum = 0;
	srand((int)time(NULL));

	for (i = 0; i < size; i++)
	{
		arr[i] = rand() % 100 + 1;
		if (i % 2)
			arr[i] = -arr[i];
	}
	printf("Array:\n");
	for (i = 0; i < size; i++)
		printf("%d ", arr[i]);
	printf("\n");

	for (i = 0; i < size; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
			maxpos = i;
		}
		if (arr[i] < min)
		{
			min = arr[i];
			minpos = i;
		}
			
	}
	if (maxpos>minpos)
	{
		for (i = minpos+1; i < maxpos; i++)
		{
			sum += arr[i];
		}

		printf("%d\n", sum);
	}
	else
	{
		for (i = maxpos+1; i <minpos; i++)
		{
			sum += arr[i];
		}

		printf("%d\n", sum);
	}
	

	return 0;
}