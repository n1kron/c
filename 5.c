#include <stdio.h>
#define WIDTH 80 

int main(){
	int len,tmp;
	int i;
	char str[WIDTH];
	printf("Please enter a string:");
	scanf(" %s", str);
    
	len=strlen(str);
    tmp=(WIDTH-len)/2;
	for(i=0;i<tmp;i++){
        printf(" ");
	}
    printf(" %s",str);
	for(i=tmp+len;i<WIDTH;i++){
        printf(" ");
	}
	return 0;
}