#include <stdio.h>

int res = 1;
int ReturnRes;
int Collt(unsigned int n)
{
	if (n == 1)
	{
		ReturnRes = res;
		res = 1;
	}	
	else
	{
		if (n % 2 == 0)
		{
			res++;
			Collt(n / 2);
		}
		else
		{
			res++;
			Collt(3 * n + 1);
		}
	}
	return ReturnRes;
}

int main()
{
	int i = 0;
	int length = 0;
	int max = 0;
	for (i = 2; i <= 1000000; i++)
	{
		if (Collt(i)>length)
		{
			length = Collt(i);
			max = i;
		}
	}
	printf("This number is %d\nLength of longest chain is %d\n", max,length);
	return 0;
}