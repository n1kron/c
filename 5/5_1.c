#include <stdio.h>
#include <time.h>
#include <windows.h>


void shift(char *begin, char *end)
{
	while (*end)
	{
		*begin++ = *end++;
	}
	*begin = 0;
};

void DeleteExtraSpaces(char *str)
{
	int i = 0;
	int j = 0;
	
	while (str[0] == ' ')
	{
		if (str[0] == ' ')
			shift(&str[0], &str[1]);
	}

	for (j = 0; j < strlen(str); j++)
	{
		for (i = 0; i < strlen(str); i++)
		{
			if (str[i] == ' ' && str[i + 1] == ' ')
				shift(&str[i], &str[i + 1]);
		}
	}
	
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = ' ';

	
	printf("String without extra spaces:\n%s\n", str);
}

int getWords(char *str,char *(*pstr)[128])
{
	int i = 0;
	int j = 0;
	int count = 0;
	int inWord = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			(*pstr)[j++] = &str[i];
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	return count;
}

void PrintWord(const char * str)
{
	int i = 0;
	while (str[i])
	{
		if (str[i] != ' ')
			printf("%c", str[i]);
		else

			break;
		i++;
	}
}




int main()
{
	
	srand((int)time(NULL));
	char str[256] = {0};
	char *pstr[128] = { 0 };
	int i = 0;
	int j = 0;
	int N = 0;
	char *tmp;
	int WordCount = 0;
	puts("Enter a string, please:");
	fgets(str, 256, stdin);
	DeleteExtraSpaces(str);
	WordCount=getWords(str, &pstr);
	N = WordCount;
	while (1)
	{
		WordCount = N;
		while (WordCount)
		{
			j = rand() % WordCount;
			PrintWord(pstr[j]);
			printf(" ");
			tmp = pstr[j];
			pstr[j] = pstr[WordCount - 1];
			pstr[WordCount - 1] = tmp;
			WordCount--;
		}
		printf("\n");
		Sleep(1000);
	}
	

	return 0;
}